#!/bin/bash

export RTE_SDK=`pwd`
export RTE_TARGET=x86_64-native-linuxapp-gcc
export EXTRA_CFLAGS=-I$(pwd)/componnents/ixvmtxrx/api/ixverify

# clean things
rm -fR build
rm -fR $RTE_TARGET

# Don't compile KNI_ETHTOOL
cat << EOF >> config/defconfig_x86_64-native-linuxapp-gcc
CONFIG_RTE_KNI_KMOD_ETHTOOL=n
CONFIG_RTE_EAL_IGB_UIO=n
CONFIG_RTE_KNI_KMOD=n
EOF

IXVMTXRX_BN=$(cat build_context.ini | grep "ixvmtxrx" | awk -F\' '{print $4}')
FTP_IXVMTXRX="ftp://robuh-cm-fs2.buh.is.keysight.com/packages/l23linecards/app/ixvmtxrx/$IXVMTXRX_BN/"
mkdir -p componnents/ixvmtxrx/
cd componnents/ixvmtxrx/
wget -m -nH --cut-dirs=5 $FTP_IXVMTXRX
cd ../../

make config T=$RTE_TARGET
make -j 4 || exit 1

mv build $RTE_TARGET

cd examples/vhost

make -j 4 || exit 1
